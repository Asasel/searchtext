//
//  SupportingConstants.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Typealiaces

typealias LightweightClosure = (() -> Void)

typealias SearchTextResultClosure = (([String]) -> Void)

// MARK: - UI

let mainScreen = UIScreen.main

let defaultOffset: CGFloat = 16.0

var topBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.size.height + (UIViewController.topViewController()?.navigationController?.navigationBar.frame.height ?? 0.0)
}

// MARK: - Other

let appDelegate = UIApplication.shared.delegate as! AppDelegate

// MARK: - Functions

func typeName(type: Any) -> String {
    return String(describing: type.self)
}

func presentInfoAlert(title: String? = nil, message: String? = nil, cancelTitle: String, completion: LightweightClosure? = nil) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { _ in
        completion?()
    })
    alertController.addAction(cancelAction)
    DispatchQueue.main.async {
        UIViewController.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}

enum SafeAreaType {
    case
    bottom,
    top,
    left,
    right
}

func countSafeAreaMargin(type: SafeAreaType) -> CGFloat {
    guard #available(iOS 11.0, *),
        let window = appDelegate.window else {
            return 0.0
    }
    switch type {
    case .top:
        return window.safeAreaInsets.top
    case .bottom:
        return window.safeAreaInsets.bottom
    case .left:
        return window.safeAreaInsets.left
    case .right:
        return window.safeAreaInsets.right
    }
}
