//
//  UIViewControllerExtension.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    static func instantiateFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>(_ viewType: T.Type) -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib(self)
    }

    class func topViewController() -> UIViewController? {
        return appDelegate.window?.visibleViewController
    }
}
