//
//  FileManagerExtension.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

extension FileManager {

    static func documentDirectory() -> URL? {
        let directoryPath = NSSearchPathForDirectoriesInDomains(SearchPathDirectory.documentDirectory, SearchPathDomainMask.userDomainMask, true).first
        guard let path = directoryPath else {
            return nil
        }
        return URL(fileURLWithPath: path)
    }

    class func isExist(path: String) -> Bool {
        return self.default.fileExists(atPath: path)
    }

    class func deleteFile(url: URL) {
        do {
            try self.default.removeItem(at: url)
        } catch {
            Log.message("FileManager: \(#function): ERROR \(error) delete file with path = \(url.path)")
        }
    }
}
