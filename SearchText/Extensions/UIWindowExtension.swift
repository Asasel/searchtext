//
//  UIWindowExtension.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {

    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(viewController: rootViewController)
    }

    public static func getVisibleViewControllerFrom(viewController: UIViewController?) -> UIViewController? {
        if let navigationController = viewController as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(viewController: navigationController.visibleViewController)
        } else if let tabBarController = viewController as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(viewController: tabBarController.selectedViewController)
        } else {
            if let pvc = viewController?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(viewController: pvc)
            } else {
                return viewController
            }
        }
    }
}
