//
//  StringExtension.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

extension String {

    func size(font: UIFont, maxWidth: CGFloat = -1, inset: CGFloat = 8.0) -> CGSize {
        let attrString = NSAttributedString.init(string: self, attributes: [NSAttributedStringKey.font: font])
        let maxSize = CGSize(width: maxWidth > 0 ? maxWidth - inset * 2 : CGFloat.greatestFiniteMagnitude,
                             height: CGFloat.greatestFiniteMagnitude)
        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)

        if maxWidth > 0 {
            return rect.size
        }
        return CGSize(width: rect.size.width + inset * 2, height: rect.size.height + inset * 2)
    }

    func findMatches(pattern: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [.anchorsMatchLines, .caseInsensitive])
            let results = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
            return results.map { String(self[Range($0.range, in: self)!]) }
        } catch {
            Log.message("\(#function): error = \(error)")
            return []
        }
    }

    func replaceCharacters(with replacement: Character) -> String {
        var someString = ""
        var someOutputPattern = self
        var index = 0
        for substring in self {
            index += 1
            if substring == replacement {
                someString.append(substring)
                if index < count {
                    continue
                }
            }
            if someString.count > 1, someOutputPattern.contains(someString) {
                someOutputPattern = someOutputPattern.replacingOccurrences(of: someString, with: "\(replacement)")
                someOutputPattern = someOutputPattern.replaceCharacters(with: replacement)
            }
            someString = ""
        }
        return someOutputPattern
    }
}
