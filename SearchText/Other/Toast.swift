//
//  Toast.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

struct ToastConstants {

    static let topMargin: CGFloat = topBarHeight + defaultOffset

    static let sideMargin: CGFloat = countSafeAreaMargin(type: .left) + countSafeAreaMargin(type: .right) + defaultOffset

    static let height: CGFloat = 35

    static let tabBarHeight: CGFloat = 64.0

    static let showTimeSeconds = TimeInterval(3)

    static let backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)

    static let textColor = UIColor.white

    static let animationDuration = 3.5
}


class Toast {

    class func toastView(messsage: String, duration: Double = ToastConstants.animationDuration, delay: Double = 0.0) {
        DispatchQueue.main.async {
            let width = UIScreen.main.bounds.width - 2 * ToastConstants.sideMargin
            let font = UIFont.systemFont(ofSize: 17.0)
            let messageHeight = messsage.size(font: font, maxWidth: width).height + defaultOffset
            let frame = CGRect(x: ToastConstants.sideMargin,
                               y: ToastConstants.topMargin,
                               width: width,
                               height: messageHeight)
            let toastLabel = UILabel(frame: frame)
            toastLabel.numberOfLines = 0
            toastLabel.backgroundColor = ToastConstants.backgroundColor
            toastLabel.textColor = ToastConstants.textColor
            toastLabel.textAlignment = NSTextAlignment.center
            toastLabel.text = messsage
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10
            toastLabel.clipsToBounds = true
            UIApplication.shared.delegate?.window??.addSubview(toastLabel)

            UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            })
        }
    }
}
