//
//  Log.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

class Log {

    // MARK: - Constants

    private static let showLog = true

    // MARK: - Construction

    class func message(_ message: String) {
        guard showLog else {
            return
        }
        print(message)
    }
}
