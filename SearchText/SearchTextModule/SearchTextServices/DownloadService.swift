//
//  DownloadService.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

class DownloadService {

    // MARK: - Constants

    let fileName = "testFile"

    let fileExtension = "txt"

    // MARK: - Properties

    private var downloadQueue: OperationQueue

    private var downloadSession: URLSession

    var fileLocation: URL {
        let documentDirectory = FileManager.documentDirectory()!
        return documentDirectory.appendingPathComponent(fileName).appendingPathExtension(fileExtension)
    }

    // MARK: - Construction

    init() {
        self.downloadSession = URLSession(configuration: .default)
        self.downloadQueue = OperationQueue()
    }

    // MARK: - Functions

    func downloadFile(url: URL, completion: @escaping LightweightClosure) {
        downloadQueue.cancelAllOperations()
        let downloadOperation = DownloadOperation(from: url, downloadSession: downloadSession)
        let saveOperation = FileSaveOperation(fileLocation: fileLocation, finishClosure: completion)
        saveOperation.addDependency(downloadOperation)
        downloadQueue.maxConcurrentOperationCount = 3
        downloadQueue.addOperations([downloadOperation, saveOperation], waitUntilFinished: false)
    }
}
