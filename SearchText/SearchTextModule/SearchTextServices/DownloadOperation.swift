//
//  DownloadOperation.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

protocol FilePassProtocol {

	var fileLocation: URL? { get }
}

class DownloadOperation: AsyncOperation, FilePassProtocol {

	// MARK: - Properties

	private var url: URL

	private var outputLocation: URL?

	private var downloadTask: URLSessionDownloadTask?

	private weak var downloadSession: URLSession?

	var fileLocation: URL? {
		return outputLocation
	}

	// MARK: - Construction

	init(from url: URL, downloadSession: URLSession) {
		self.url = url
		self.downloadSession = downloadSession
		super.init()
	}

	// MARK: - Functions

	override func main() {
		guard !isCancelled else {
			state = .finished
			return
		}
		downloadFile(from: url, completion: { [weak self]  (location) in
			guard let strongSelf = self else {
				return
			}
			strongSelf.outputLocation = location
			strongSelf.state = .finished
		})
	}

	override func cancel() {
		downloadTask?.cancel()
		super.cancel()
	}

	private func downloadFile(from url: URL, completion: @escaping ((URL?) -> Void)) {
		downloadTask = downloadSession?.downloadTask(with: url) { (location, _, error) -> Void in
			guard let location = location, error == nil else {
				completion(nil)
				return
			}
			completion(location)
		}
		downloadTask?.resume()
	}
}
