//
//  SaveSearchResultsService.swift
//  SearchText
//
//  Created by Asasel on 07.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

class SaveSearchResultsService {

    // MARK: - Nested

    struct SaveSearchResultsConstants {

        static let fileName = "results"

        static let fileExtension = "log"

        static let nextRow = "\n"
    }

    // MARK: - Properties

    var logPath: URL {
        let documentDirectory = FileManager.documentDirectory()!
        return documentDirectory.appendingPathComponent(SaveSearchResultsConstants.fileName).appendingPathExtension(SaveSearchResultsConstants.fileExtension)
    }

    // MARK: - Functions

    func createLogFile() {
        if FileManager.isExist(path: logPath.path) {
            Log.message("SaveSearchResultsService: \(#function): file already exists")
            return
        }
        do {
            try "".write(to: logPath, atomically: true, encoding: .ascii)
        } catch let error {
            Log.message("SaveSearchResultsService: \(#function): error = \(error)")
        }
    }

    func saveResults(results: [String]) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            results.forEach { resultString in
                let resultRow = "\(resultString)\(SaveSearchResultsConstants.nextRow)"
                if let data = resultRow.data(using: String.Encoding.ascii),
                    let fileHandle = try? FileHandle(forUpdating: strongSelf.logPath) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                }
            }
        }
    }
}
