//
//  FileSaveOperation.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

class FileSaveOperation: AsyncOperation {

    // MARK: - Constants

    let folderName = "SearchTextFile"

	// MARK: - Properties

	private var inputLocation: URL?

	private var locationToSave: URL? {
		var location: URL?
		if let inputLocation = inputLocation {
			return inputLocation
		}
		if let dataProvider = dependencies
			.filter({ $0 is FilePassProtocol })
			.first as? FilePassProtocol {
			location = dataProvider.fileLocation
		}
		return location
	}

    private var fileLocation: URL

    var finishClosure: LightweightClosure

    // MARK: - Construction

    init(fileLocation: URL, finishClosure: @escaping LightweightClosure) {
        self.fileLocation = fileLocation
        self.finishClosure = finishClosure
    }

	// MARK: - Functions

	override func main() {
		guard !isCancelled else {
			endActions()
			return
		}
		guard let location = locationToSave else {
			endActions()
			return
		}
		saveFile(location: location)
	}

	private func saveFile(location: URL) {
        if FileManager.isExist(path: fileLocation.path) {
            print("FileSaveOperation: re-save file")
            FileManager.deleteFile(url: fileLocation)
            endActions()
        }
        do {
            try FileManager.default.moveItem(at: location, to: fileLocation)
        } catch let error {
            print("FileSaveOperation: error = \(error.localizedDescription)")
            return
        }
        endActions()
	}

    private func endActions() {
        finishClosure()
        state = .finished
    }
}
