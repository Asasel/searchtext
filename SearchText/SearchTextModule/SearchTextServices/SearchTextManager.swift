//
//  SearchTextManager.swift
//  SearchText
//
//  Created by Asasel on 05.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

enum SearchManageSymbols: Character {
    case
    unlimitedCharacters = "*",
    oneCharacter = "?",
    startString = "^",
    endString = "$"

    static func regexPattern(patternCharacter: Character) -> String {
        switch patternCharacter {
        case SearchManageSymbols.unlimitedCharacters.rawValue:
            return "(.*)"
        case SearchManageSymbols.oneCharacter.rawValue:
            return "(.)"
        default:
            return ""
        }
    }
}

class SearchTextManager {

    // MARK: - Functions

    func findText(_ pattern: String, filePath: String, completion: @escaping SearchTextResultClosure) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            let fileText = strongSelf.text(from: filePath)
            let validatedPattern = strongSelf.validatePattern(pattern)
            let result = fileText.findMatches(pattern: validatedPattern)
            completion(result)
        }
    }

    private func text(from filePath: String) -> String {
        guard let fileText = try? String(contentsOfFile: filePath, encoding: String.Encoding.ascii) else {
            return ""
        }
        return fileText
    }

    private func validatePattern(_ pattern: String) -> String {
        var outputPattern = pattern.replaceCharacters(with: SearchManageSymbols.unlimitedCharacters.rawValue)
        outputPattern = outputPattern.replacingOccurrences(of: "\(SearchManageSymbols.unlimitedCharacters.rawValue)", with: SearchManageSymbols.regexPattern(patternCharacter: SearchManageSymbols.unlimitedCharacters.rawValue))
        outputPattern.insert(SearchManageSymbols.startString.rawValue, at: outputPattern.startIndex)
        outputPattern = outputPattern.replacingOccurrences(of: "\(SearchManageSymbols.oneCharacter.rawValue)", with: SearchManageSymbols.regexPattern(patternCharacter: SearchManageSymbols.oneCharacter.rawValue))
        outputPattern.append(SearchManageSymbols.endString.rawValue)
        return outputPattern
    }
}
