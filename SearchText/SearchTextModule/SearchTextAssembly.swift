//
//  SearchTextAssembly.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

class SearchTextAssembly {

    // MARK: - Properties

    private(set) var searchTextViewController: SearchTextViewController

    private var presenter: SearchTextPresenter

    private var interactor: SearchTextInteractor

    // MARK: - Constructor

    init() {
        self.searchTextViewController = SearchTextViewController.instantiateFromNib()
        self.interactor = SearchTextInteractor()
        self.presenter = SearchTextPresenter(contentViewController: self.searchTextViewController, interactor: self.interactor)
        self.searchTextViewController.presenter = self.presenter
    }
}
