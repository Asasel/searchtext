//
//  SearchTextPresenter.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

enum SearchTextUpdateType {
    case
    firstAppear,
    searchAndUpdate
}

class SearchTextPresenter {

    // MARK: - Properties

    private weak var contentViewController: SearchTextViewController?

    private weak var interactor: SearchTextInteractor?

    private var resultCellViewModels: [ResultCellViewModel]

    var searchTextViewModel: SearchTextViewModel {
        return SearchTextViewModel(tableHeaderTitle: "Search results", resultCellViewModels: resultCellViewModels)
    }

    var isFileDownloaded: Bool {
        return interactor?.isDownloaded ?? false
    }


    // MARK: - Constructor

    init(contentViewController: SearchTextViewController, interactor: SearchTextInteractor) {
        self.contentViewController = contentViewController
        self.interactor = interactor
        self.resultCellViewModels = [ResultCellViewModel]()
    }

    // MARK: - Functions

    func updateContentView(type: SearchTextUpdateType) {
        contentViewController?.viewModel = searchTextViewModel
        switch type {
        case .firstAppear:
            contentViewController?.updateContentView()
        case .searchAndUpdate:
            contentViewController?.updateWithSearchResults()
        }
    }

    func performDownload(urlString: String) {
        guard let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) else {
            presentInfoAlert(title: "Error", message: "invalid url string!", cancelTitle: "OK")
            return
        }
        contentViewController?.activityIndicator.startAnimating()
        interactor?.downloadText(url: url, completion: {
            Toast.toastView(messsage: "Text file was successfuly downloaded")
            DispatchQueue.main.async { [weak self] in
                self?.contentViewController?.activityIndicator.stopAnimating()
                self?.updateControls()
                self?.cleanModels()
            }
        })
    }

    func performSearchText(_ text: String) {
        guard isFileDownloaded else {
            presentInfoAlert(title: "Error", message: "File does not exist!", cancelTitle: "Close")
            cleanModels()
            return
        }
        contentViewController?.activityIndicator.startAnimating()
        interactor?.searchText(text, completion: { [weak self] searchResults in
            guard let strongSelf = self else {
                return
            }
            strongSelf.resultCellViewModels = searchResults.compactMap { ResultCellViewModel(searchResult: $0) }
            DispatchQueue.main.async { [weak self] in
                self?.updateContentView(type: .searchAndUpdate)
                self?.contentViewController?.activityIndicator.stopAnimating()
            }
        })
    }

    func updateControls() {
        contentViewController?.searchTextField.isEnabled = isFileDownloaded
        contentViewController?.searchButton.isEnabled = isFileDownloaded
    }

    private func cleanModels() {
        resultCellViewModels.removeAll()
        updateContentView(type: .searchAndUpdate)
    }
}
