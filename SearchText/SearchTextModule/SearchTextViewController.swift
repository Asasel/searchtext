//
//  SearchTextViewController.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

class SearchTextViewController: UIViewController {

    // MARK: - Constants

    let cellIdentifier = typeName(type: ResultTableViewCell.self)

    let offset: CGFloat = 16.0

    // MARK: - Properties

    @IBOutlet private weak var urlTextField: UITextField!

    @IBOutlet private(set) weak var searchTextField: UITextField!

    @IBOutlet private weak var downloadButton: UIButton!

    @IBOutlet private(set) weak var searchButton: UIButton!

    @IBOutlet private weak var resultsTableView: UITableView!

    @IBOutlet private(set) weak var noResultsLabel: UILabel!

    @IBOutlet private(set) weak var activityIndicator: UIActivityIndicatorView!

    var viewModel = SearchTextViewModel.emptyModel

    weak var presenter: SearchTextPresenter?

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        resultsTableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        resultsTableView.estimatedRowHeight = 44.0
        resultsTableView.estimatedSectionHeaderHeight = 50.0
        resultsTableView.dataSource = self
        resultsTableView.tableFooterView = UIView()
        presenter?.updateContentView(type: .firstAppear)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.updateControls()
    }

    // MARK: - IBActions

    @IBAction func downloadButtonTouch(_ sender: Any) {
        view.endEditing(true)
        guard let urlString = urlTextField.text, !urlString.isEmpty else {
            presentInfoAlert(title: "Error", message: "URL field must be filled!", cancelTitle: "OK")
            return
        }
        presenter?.performDownload(urlString: urlString)
    }

    @IBAction func searchText(_ sender: Any) {
        view.endEditing(true)
        guard let searchText = searchTextField.text, !searchText.trimmingCharacters(in: .whitespaces).isEmpty else {
            presentInfoAlert(title: "Error", message: "Search text field must be filled!", cancelTitle: "OK")
            return
        }
        presenter?.performSearchText(searchText)
    }

    // MARK: - Functions

    func updateContentView() {
        urlTextField.text = SearchTextViewModel.defaultTextURL
        updateWithSearchResults()
    }

    func updateWithSearchResults() {
        noResultsLabel.text = "Nothing found"
        noResultsLabel.isHidden = !viewModel.resultCellViewModels.isEmpty
        resultsTableView.reloadData()
    }
}

extension SearchTextViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.resultCellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResultTableViewCell
        let cellViewModel = viewModel.resultCellViewModels[indexPath.row]
        cell.resultLabel.text = cellViewModel.searchResult
        return cell
    }
}

extension SearchTextViewController: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        Log.message("SearchTextViewController: \(#function): text = \(String(describing: textField.text))")
    }
}
