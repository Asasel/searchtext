//
//  SearchTextInteractor.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

class SearchTextInteractor {

    // MARK: - Properties

    private var downloadService: DownloadService

    private var searchTextManager: SearchTextManager

    private var saveSearchResultsService: SaveSearchResultsService

    var isDownloaded: Bool {
        return FileManager.isExist(path: downloadService.fileLocation.path)
    }

    // MARK: - Constructor

    init() {
        self.downloadService = DownloadService()
        self.searchTextManager = SearchTextManager()
        self.saveSearchResultsService = SaveSearchResultsService()
    }

    // MARK: - Functions

    func downloadText(url: URL, completion: @escaping LightweightClosure) {
        downloadService.downloadFile(url: url, completion: {
            completion()
        })
    }

    func searchText(_ text: String, completion: @escaping SearchTextResultClosure) {
        searchTextManager.findText(text, filePath: downloadService.fileLocation.path) { [weak self] results in
            self?.saveResults(results)
            completion(results)
        }
    }

    func saveResults(_ results: [String]) {
        saveSearchResultsService.createLogFile()
        saveSearchResultsService.saveResults(results: results)
    }
}
