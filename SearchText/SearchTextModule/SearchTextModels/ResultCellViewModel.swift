//
//  ResultCellViewModel.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

struct ResultCellViewModel {

    let searchResult: String
}
