//
//  SearchTextViewModel.swift
//  SearchText
//
//  Created by Asasel on 03.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation

struct SearchTextViewModel {

    static let emptyModel = SearchTextViewModel(tableHeaderTitle: "", resultCellViewModels: [])

    let tableHeaderTitle: String

    static let defaultTextURL = "https://jimmunroe.net/everyoneinsilico.txt"

    var resultCellViewModels: [ResultCellViewModel]
}
