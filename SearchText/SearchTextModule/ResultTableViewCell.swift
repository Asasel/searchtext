//
//  ResultTableViewCell.swift
//  SearchText
//
//  Created by Asasel on 02.08.2018.
//  Copyright © 2018 Asasel. All rights reserved.
//

import Foundation
import UIKit

class ResultTableViewCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet weak var resultLabel: UILabel!

}
