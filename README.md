#  Search Text README 

SearchText iOS App:

-   loads a text file from a source: "https://jimmunroe.net/everyoneinsilico.txt"
-   selects lines matching the filter
-   displays them in a list via tableview


Here a custom regex was implemented using symbols * and ?:
"*"   -   a sequence of any characters of unlimited length, 
"?" -   any one character.

For example:

-   The mask * abc* selects all lines containing abc and beginning and ending with any sequence of characters;
-   The abc* mask selects all lines starting with abc and ending with any sequence of characters;
-   The mask abc? selects all lines starting with abc and ending with any additional character;
-   The abc mask selects all rows that are equal to this mask;
-   There are no restrictions on the position * in the mask

